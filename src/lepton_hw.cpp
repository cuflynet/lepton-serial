/* Base device object */

#include <lepton_serial/lepton_hw.h>

LeptonDriver::LeptonDriver()
{
    //Assumes that this is a fresh start of the interface
    shouldStop = false;
    mPort = NULL;
}

LeptonDriver::~LeptonDriver()
{
    ///Stop service thread
    shouldStop = true;
    if (serviceThread) {
        serviceThread->join();
        delete serviceThread;
      }
    if (mPort) {
        delete mPort;
    }
}

int LeptonDriver::initFake()
{
    //Set up a fake image provider
    serviceThread = new thread(bind(&LeptonDriver::serviceFake, this));
  
    return LEPTON_SUCCESS;
}

int LeptonDriver::init(string portName, int baud)
{
    //Attempt to open serial device:
    try {
        mPort = new serial::Serial(portName, baud, serial::Timeout::simpleTimeout(LEPTON_TIMEOUT));
    } catch (serial::IOException e) {
        cout << __FILE__ << ": Unable to open port:" << portName << endl;
        return LEPTON_FAILURE;
    }

    if (!mPort->isOpen()) {
        //Unable to open serial port for some reason
        cout <<  __FILE__ << ": Unable to open port:" << portName << endl;
        return LEPTON_FAILURE;
    }

    //Start the service thread and kick this thing off
    serviceThread = new thread(bind(&LeptonDriver::service, this));
    return LEPTON_SUCCESS;
}

void LeptonDriver::service()
{
    //Speak the serial protocol and fill the queue with images

    uint16_t *frameBuffer;
    uint32_t i;
    cout << __FILE__ << "Service thread started" << endl;
    while (!shouldStop) {
        //Read a line from the thing
        string rawResult = mPort->readline();
        //TODO: Check for timeout
        //Parse the line into a char** of the values
        vector<string> tokens = split(rawResult, string(" "));
        //cout << "Got " << tokens.size() << " tokens" << endl;
        //Translate each token into a uint8_t value:
        frameBuffer = new uint16_t[tokens.size()];
        for (i =0; i<tokens.size(); i++) {
            uint32_t temp = strtoul(tokens[i].c_str(),NULL,16);
        if (temp > UINT16_MAX) {
                cout << __FILE__ << ": Excessively long token:" << tokens[i] << endl;
            frameBuffer[i] = 0;
            } else {
            frameBuffer[i] = temp;
            }
        }
        //Create a new image with the given buffer:
        queueLock.lock();
        uint16_t height = 60;
        uint16_t width = 80;
        images.push(new IRFrame(width, height, &frameBuffer[0]));
        queueLock.unlock();
    }
    return;
}

void LeptonDriver::serviceFake()
{
    //Publish fake images
    uint16_t *frameBuffer;
    uint32_t i;
    uint32_t height = 60;
    uint32_t width = 80;
    printf("Fake service started\n");
    while (!shouldStop) {
        //Make a fake frame
        frameBuffer = new uint16_t[height*width*sizeof(uint16_t)];

        for (i=0; i<height*width; i++) {
      frameBuffer[i] = 255;
    }
      
        //Create a new image with the given buffer:
        printf("Pushing frame\n");
        queueLock.lock();
        images.push(new IRFrame(width, height, frameBuffer));
        queueLock.unlock();
        delete[] frameBuffer;
        usleep(100000);
    }
    return;
}

int LeptonDriver::getFrame(IRFrame* &frame)
{
    //Return LEPTON_SUCCESS if there's a frame - snag it from the queue and fill the buffer
    //Otherwise, return LEPTON_FAILURE
    queueLock.lock();
    if (images.size() == 0) {
        frame = 0;
        queueLock.unlock();
        return LEPTON_FAILURE;
    } else {
        frame = images.front();
        images.pop();
        queueLock.unlock();
        return LEPTON_SUCCESS;
    }
}
