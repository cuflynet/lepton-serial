/*
 * trackerMain.cpp
 *
 *  Created on: Oct 6, 2015
 *      Author: taylordean
 */

#include <iostream>
#include <cmath>
#include <chrono>
#include <thread>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <std_msgs/MultiArrayLayout.h>
#include <std_msgs/MultiArrayDimension.h>
#include <std_msgs/Float32MultiArray.h>
#include <cv_bridge/cv_bridge.h>
#include <target_tracker/trackerMain.hpp>
#include <target_tracker/OBJECT.hpp>
#include <target_tracker/OBJECT_CONTAINER.hpp>

#define DEBUG 0

using namespace cv;
using namespace std;

auto START_TIME = chrono::high_resolution_clock::now();
bool CALIBRATION_COMPLETE = false;
bool READING_DEPTH_IMAGE = false;
bool READING_IR_IMAGE = false;
bool SETTING_DEPTH_IMAGE = false;
bool SETTING_IR_IMAGE = false;
bool SUBSCRIBERS_STARTED = false;
bool DEPTH_IMAGE_UPDATED = false;
bool IR_IMAGE_UPDATED = false;
int INTENSITY_UPPER_BOUND = 65535;
int INTENSITY_LOWER_BOUND = 0;
float RAW_INTENSITY = 0;
Mat DEPTH_IMAGE;
Mat IR_IMAGE;

void getDepthImage(const sensor_msgs::ImageConstPtr& msg)
{
    if (!READING_DEPTH_IMAGE) {
        SETTING_DEPTH_IMAGE = true;
        cv_bridge::CvImagePtr cv_ptr;
        try {
            cv_bridge::CvImagePtr cvPtr = cv_bridge::toCvCopy(msg, "");
            DEPTH_IMAGE = cvPtr->image.clone();
            DEPTH_IMAGE_UPDATED = true;
        } catch (cv_bridge::Exception &e) {
            ROS_ERROR("Could not convert depth image from '%s' to 'mono16'.", msg->encoding.c_str());
        }
        SETTING_DEPTH_IMAGE = false;
    }
}

void getFlirImage(const sensor_msgs::ImageConstPtr& msg)
{
    if (!READING_IR_IMAGE) {
        SETTING_IR_IMAGE = true;
        cv_bridge::CvImagePtr cv_ptr;
        try {
            cv_bridge::CvImagePtr cvPtr = cv_bridge::toCvCopy(msg, "mono16");
            IR_IMAGE = cvPtr->image.clone();
            IR_IMAGE_UPDATED = true;
        } catch (cv_bridge::Exception &e) {
            ROS_ERROR("Could not convert flir image from '%s' to 'mono16'.", msg->encoding.c_str());
        }
        SETTING_IR_IMAGE = false;
    }
    SUBSCRIBERS_STARTED = true;
}

void applyGain(Mat imgIn, Mat imgOut)
{
    float minValue = 65536;
    float maxValue = 0;

    for (int rowIdx = 0; rowIdx < imgIn.rows; rowIdx++) {
        for (int colIdx = 0; colIdx < imgIn.cols; colIdx++) {
            float val = imgIn.at<uint16_t>(rowIdx, colIdx);
            if (val > maxValue) maxValue = val;
            if (val < minValue) minValue = val;
        }
    }
    
    float diff = maxValue - minValue + 1;
    for (int rowIdx = 0; rowIdx < imgIn.rows; rowIdx++) {
        for (int colIdx = 0; colIdx < imgIn.cols; colIdx++) {
            float val = imgIn.at<uint16_t>(rowIdx, colIdx);
            uint16_t newVal = (65536.0*(val - minValue)/diff);
            imgOut.at<uint16_t>(rowIdx, colIdx) = newVal;
        }
    }
}

void morphImage(Mat imgIn, Mat& imgOut, int iLow, int iHigh)
{
    inRange(imgIn, iLow, iHigh, imgOut);

    //morphological opening (removes small objects from the foreground)
    erode(imgOut, imgOut, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
    dilate(imgOut, imgOut, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));

    //morphological closing (removes small holes from the foreground)
    dilate(imgOut, imgOut, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
    erode(imgOut, imgOut, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
}

void getContours(OBJECT_CONTAINER& objects, Mat imgContour)
{
    vector< vector < Point > > contours;
    vector< vector < Point > > finalContours;
    vector< double > areas;
    time_t gmt = time(0);
    findContours(imgContour, contours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE);

    for (unsigned int contourIdx=0; contourIdx<contours.size(); contourIdx++) {
        double area = contourArea(contours[contourIdx], false);
        if (area > MIN_OBJECT_AREA) {
            areas.push_back(area);
            finalContours.push_back(contours[contourIdx]);
        }
    }

    for (int retIdx=0; retIdx<MAX_OBJECTS; retIdx++) {
        if (finalContours.empty()) {
            break;
        } else {
            int maxIdx = 0;
            for (unsigned int contourIdx=0; contourIdx<finalContours.size(); contourIdx++) {
                if (areas[maxIdx] < areas[contourIdx]) {
                    maxIdx = contourIdx;
                }
            }
            Point centroid;
            findCentroid(finalContours[maxIdx], centroid);
            Rect brect = boundingRect(Mat(finalContours[maxIdx]).reshape(2));

            OBJECT newObject(objects.size(), -1, gmt, areas[maxIdx], centroid, brect);
            objects.append(newObject);

            finalContours.erase(finalContours.begin() + maxIdx);
            areas.erase(areas.begin() + maxIdx);
        }
    }
}

void findCentroid(vector< Point > points, Point& centroid)
{
    float sumx = 0;
    float sumy = 0;
    for (unsigned int i=0; i<points.size(); i++) {
        sumx = sumx + points[i].x;
        sumy = sumy + points[i].y;
    }
    int posX = sumx / points.size();
    int posY = sumy / points.size();

    centroid = Point(posX, posY);
}

void processImages(Mat imgDepth, Mat imgOriginal, Mat& imgThresholded, OBJECT_CONTAINER& GLOBAL_RETURNS)
{
    morphImage(imgOriginal, imgThresholded, INTENSITY_LOWER_BOUND, INTENSITY_UPPER_BOUND);

    getContours(GLOBAL_RETURNS, imgThresholded.clone());

    for (int trkIdx=0; trkIdx<GLOBAL_RETURNS.size(); trkIdx++) {
        unsigned int xloc = GLOBAL_RETURNS[trkIdx].centroid.x + 1;
        unsigned int yloc = GLOBAL_RETURNS[trkIdx].centroid.y + 1;

        // UPDATE THIS WHEN MORE IS KNOWN ABOUT IMAGES
        double az = FLIR_HFOV/2 - xloc*FLIR_HFOV/imgOriginal.cols;
        double el = FLIR_VFOV/2 - yloc*FLIR_VFOV/imgOriginal.rows;
        unsigned int depthy = -(az - 0.5*DEPTH_HFOV)*imgDepth.cols/DEPTH_HFOV;
        unsigned int depthx = -(el - 0.5*DEPTH_VFOV)*imgDepth.rows/DEPTH_VFOV;

        double dist = imgDepth.at<float>(depthx, depthy);
        GLOBAL_RETURNS[trkIdx].az = az;
        GLOBAL_RETURNS[trkIdx].el = el;
        GLOBAL_RETURNS[trkIdx].range = dist;
        GLOBAL_RETURNS[trkIdx].xpos = -dist*cos(el)*sin(az);
        GLOBAL_RETURNS[trkIdx].ypos = dist*cos(el)*cos(az);
        GLOBAL_RETURNS[trkIdx].zpos = dist*sin(el);
        GLOBAL_RETURNS[trkIdx].depthLoc.x = depthy;
        GLOBAL_RETURNS[trkIdx].depthLoc.y = depthx;
        GLOBAL_RETURNS[trkIdx].flirLoc.x = yloc;
        GLOBAL_RETURNS[trkIdx].flirLoc.y = xloc;
    }
}

void doTracking(image_transport::Publisher irTrackedPub, image_transport::Publisher irMaskedPub, image_transport::Publisher depthTrackedPub, ros::Publisher targetPosPub)
{
    while (true) {
        if (!SUBSCRIBERS_STARTED || !DEPTH_IMAGE_UPDATED || !IR_IMAGE_UPDATED) {
            if (DEBUG == 1) {
                fprintf(stdout, "SUBSCRIBERS_STARTED=%u\tDEPTH_IMAGE_UPDATED=%u\tIR_IMAGE_UPDATED=%u\n", SUBSCRIBERS_STARTED, DEPTH_IMAGE_UPDATED, IR_IMAGE_UPDATED);
                fflush(stdout);
            }
            continue;
        }

        // ALLOCATE OBJECT_CONTAINER
        OBJECT_CONTAINER GLOBAL_RETURNS;

        // READ IN THERMAL IMAGE AND DEPTH IMAGE
        Mat imgDepth;
        Mat imgOriginal;

        if (!SETTING_DEPTH_IMAGE) {
            READING_DEPTH_IMAGE = true;
            imgDepth = DEPTH_IMAGE.clone();
            READING_DEPTH_IMAGE = false;
            DEPTH_IMAGE_UPDATED = false;
        } else {
            continue;
        }

        if (!SETTING_IR_IMAGE) {
            READING_IR_IMAGE = true;
            imgOriginal = IR_IMAGE.clone();
            READING_IR_IMAGE = false;
            IR_IMAGE_UPDATED = false;
        } else {
            continue;
        }

        // PERFORM CALIBRATION
        if (!CALIBRATION_COMPLETE) {
            RAW_INTENSITY = mean(imgOriginal).val[0];
            CALIBRATION_COMPLETE = true;
            fprintf(stdout, "Averaged unscaled human intensity = %f\n", RAW_INTENSITY);
            fflush(stdout);

            INTENSITY_LOWER_BOUND = RAW_INTENSITY - 100;
            INTENSITY_UPPER_BOUND = RAW_INTENSITY + 100;
        }

        // DO TARGET DETECTION
        Mat imgThresholded(FLIR_HPX, FLIR_VPX, CV_16UC1);
        processImages(imgDepth, imgOriginal, imgThresholded, GLOBAL_RETURNS);
    
        Mat trackedDepth;
        imgDepth.convertTo(trackedDepth, CV_16U);
        applyGain(imgOriginal, imgOriginal);
        applyGain(trackedDepth, trackedDepth);

        // OUPUT THE TARGET POSITIONS AND DRAW RECTANGLES FOR VISUALIZATION
        for (int trkIdx=0; trkIdx<GLOBAL_RETURNS.size(); trkIdx++) {
            circle(trackedDepth, GLOBAL_RETURNS[trkIdx].depthLoc, 15, 0, 4);
            circle(trackedDepth, GLOBAL_RETURNS[trkIdx].depthLoc, 20, 65535, 4);
            rectangle(imgOriginal, GLOBAL_RETURNS[trkIdx].brect.tl(), GLOBAL_RETURNS[trkIdx].brect.br(), 65535, 2, CV_AA);
            rectangle(imgThresholded, GLOBAL_RETURNS[trkIdx].brect.tl(), GLOBAL_RETURNS[trkIdx].brect.br(), 65535, 2, CV_AA);
            rectangle(imgOriginal, GLOBAL_RETURNS[trkIdx].brect.tl(), GLOBAL_RETURNS[trkIdx].brect.br(), 0, 1, CV_AA);
            rectangle(imgThresholded, GLOBAL_RETURNS[trkIdx].brect.tl(), GLOBAL_RETURNS[trkIdx].brect.br(), 0, 1, CV_AA);
        }

        // PUBLISH DEBUG IMAGES TO ROS TOPICS
        sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "mono8", imgThresholded).toImageMsg();
        irMaskedPub.publish(msg);
        msg = cv_bridge::CvImage(std_msgs::Header(), "mono16", imgOriginal).toImageMsg();
        irTrackedPub.publish(msg);
        msg = cv_bridge::CvImage(std_msgs::Header(), "mono16", trackedDepth).toImageMsg();
        depthTrackedPub.publish(msg);

        // OUPUT THE TARGET POSITIONS AND DRAW RECTANGLES FOR VISUALIZATION
        std_msgs::Float32MultiArray pos;
        pos.data.clear();
        for (int trkIdx=0; trkIdx<GLOBAL_RETURNS.size(); trkIdx++) { 
            pos.data.push_back(GLOBAL_RETURNS[trkIdx].xpos);
            pos.data.push_back(GLOBAL_RETURNS[trkIdx].ypos); 
            pos.data.push_back(GLOBAL_RETURNS[trkIdx].zpos);

            fprintf(stdout, "trackID = %d\txpos = %2.3f\typos = %2.3f\tzpos = %2.3f\n", trkIdx, GLOBAL_RETURNS[trkIdx].xpos, GLOBAL_RETURNS[trkIdx].ypos, GLOBAL_RETURNS[trkIdx].zpos);
        }
        targetPosPub.publish(pos);
        ros::spinOnce();
    }
}

int main(int argc, char** argv)
{
    if (!CALIBRATION_COMPLETE) {
        fprintf(stdout, "\n\n\nTime to calibrate the camera.\n"
                "Hold object of desired temperature\n"
                "in front of the camera so that it \n"
                "completely obscures the lens.\n\n");
        fflush(stdout);
        usleep(3*1000000);

        fprintf(stdout, "Calbration beginning in 3 seconds...\n");
        fflush(stdout);

        usleep(1*1000000);
        fprintf(stdout, "3...");
        fflush(stdout);

        usleep(1*1000000);
        fprintf(stdout, "2...");
        fflush(stdout);

        usleep(1*1000000);
        fprintf(stdout, "1...");
        fflush(stdout);

        usleep(1*1000000);
        fprintf(stdout, "\nCalibrating...\n");
        fflush(stdout);
        usleep(1*1000000);
    }

    // INITIATE PUBLISHERS
    ros::init(argc, argv, "image_listener");
    ros::NodeHandle nh;
    image_transport::ImageTransport it(nh);

    image_transport::Publisher irTrackedPub = it.advertise("camera/tracking/IR_Tracked", 1);
    image_transport::Publisher irMaskedPub = it.advertise("camera/tracking/IR_Masked", 1);
    image_transport::Publisher depthTrackedPub = it.advertise("camera/tracking/Depth_Tracked", 1);

    ros::Publisher targetPosPub = nh.advertise<std_msgs::Float32MultiArray>("tracking/target_pos", 1);

    waitKey(30);

    thread doTrackingThread(doTracking, irTrackedPub, irMaskedPub, depthTrackedPub, targetPosPub);

    // INITIATE IMAGE SUBSCRIBER
    image_transport::Subscriber flirSub = it.subscribe("camera/flir/image", 1, getFlirImage);
    image_transport::Subscriber depthSub = it.subscribe("camera/depth/image", 1, getDepthImage);

    ros::spin();

    
    return 0;
}
