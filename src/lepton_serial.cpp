/* Test the Lepton serial device object */
#include <string>
#include <iostream>
#include <stdlib.h>
#include <signal.h>
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>

#include <lepton_serial/lepton_hw.h>

using namespace cv;
using namespace std;

int shouldQuit = 0;

void sigHandler(int signum)
{
    shouldQuit = 1;
    printf("Caught SIGINT %d, exiting\n", signum);
}

int main(int argc, char** argv)
{
    LeptonDriver lepton;
    string portName;
    int baud = 230400;
  
    if (argc > 2) {
        portName = string(argv[1]);
    } else {
        portName = string("/dev/ttyACM0");
    }
  
    if (argc > 3) { 
        baud = atoi(argv[2]);
    }
 
    int ret = lepton.init(portName, baud);
    //int ret = lepton.initFake();
    if (ret == lepton.LEPTON_FAILURE) {
        printf("Unable to init lepton on port: %s\n", portName.c_str());
        exit(1);
    }

    //install signal handler
    signal(SIGINT, sigHandler);

    // Setup image publisher
    // Translate to a CV_MAT and use imshow() to display
    ros::init(argc, argv, "image_transport");
    ros::NodeHandle nh;
    image_transport::ImageTransport it(nh);
    image_transport::Publisher pub = it.advertise("camera/flir/image", 1);
    waitKey(30);

    while (!shouldQuit) {
        IRFrame *theFrame = NULL;
        int ret = lepton.getFrame(theFrame);
        if (ret == lepton.LEPTON_SUCCESS) {
            Mat rawIR(theFrame->height, theFrame->width, CV_16UC1, theFrame->data);
            sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "mono16", rawIR).toImageMsg();          
            pub.publish(msg);
            ros::spinOnce();
        delete theFrame;
    }
        usleep(0);
    }
}


