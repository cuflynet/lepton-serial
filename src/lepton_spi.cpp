#include <string>
#include <iostream>
#include <stdlib.h>
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include "lepton_spi/SPI.h"
#include "lepton_spi/Lepton_I2C.h"

#define PACKET_SIZE 164
#define PACKET_SIZE_UINT16 (PACKET_SIZE/2)
#define PACKETS_PER_FRAME 60
#define FRAME_SIZE_UINT16 (PACKET_SIZE_UINT16*PACKETS_PER_FRAME)
#define FPS 27;
#define FLIR_HEIGHT 60
#define FLIR_WIDTH 80

using namespace cv;
using namespace std;

int main( int argc, char **argv )
{
    ros::init(argc, argv, "image_transport");
    ros::NodeHandle nh;
    image_transport::ImageTransport it(nh);
    image_transport::Publisher pub = it.advertise("camera/flir/image", 1);
    waitKey(30);

    //open spi port
    SpiOpenPort(0);
    uint8_t result[PACKET_SIZE*PACKETS_PER_FRAME];
    uint16_t *frameBuffer;
        while(true) {
            //read data packets from lepton over SPI
            int resets = 0;
            for(int j=0;j<PACKETS_PER_FRAME;j++) {
                //if it's a drop packet, reset j to 0, set to -1 so he'll be at 0 again loop
                read(spi_cs0_fd, result+sizeof(uint8_t)*PACKET_SIZE*j, sizeof(uint8_t)*PACKET_SIZE);
                int packetNumber = result[j*PACKET_SIZE+1];
                if(packetNumber != j) {
                    j = -1;
                    resets += 1;
                    usleep(1000);
                    //Note: we've selected 750 resets as an arbitrary limit, since there should never be 750 "null" packets between two valid transmissions at the current poll rate
                    //By polling faster, developers may easily exceed this count, and the down period between frames may then be flagged as a loss of sync
                    if(resets == 750) {
                        SpiClosePort(0);
                        usleep(750000);
                        SpiOpenPort(0);
                    }
                }
            }

            frameBuffer = (uint16_t *)result;
            int row, column;
            uint16_t value;

            Mat rawIR(FLIR_HEIGHT, FLIR_WIDTH, CV_16UC1, 10);
            for(int i=0;i<FRAME_SIZE_UINT16;i++) {
                //skip the first 2 uint16_t's of every packet, they're 4 header bytes
                if(i % PACKET_SIZE_UINT16 < 2) {
                    continue;
                }
                //flip the MSB and LSB at the last second
                int temp = result[i*2];
                result[i*2] = result[i*2+1];
                result[i*2+1] = temp;

                value = frameBuffer[i];
                column = i % PACKET_SIZE_UINT16 - 2;
                row = i / PACKET_SIZE_UINT16;
                rawIR.at<uint16_t>(row, column) = value;
            }

            //lets emit the signal for update
            sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "mono16", rawIR).toImageMsg();
            pub.publish(msg);
            ros::spinOnce();
        }

        //finally, close SPI port just bcuz
        SpiClosePort(0);

    return 0;
}

