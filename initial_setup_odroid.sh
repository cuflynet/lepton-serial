# Fix Mouse Speed
echo "Fixing mouse lag problem..."
sleep 5
sudo echo "dwc_otg.lpm_enable=0 console=ttyAMA0,115200 console=tty1 root=/dev/mmcblk0p7 rootfstype=ext4 elevator=deadline fsck.repair=yes rootwait usbhid.mousepoll=0" > /boot/cmdline.txt

# Install vim, ROS, cv_bridge, openni, rqt, and image_transport
echo "Performing updates"
sleep 5
sudo apt-get -y update
sudo apt-get -y upgrade
sudo apt-get -y dist-upgrade
sudo rpi-update

echo "Installing vim..."
sleep 5
sudo apt-get install -y vim

echo "Installing ROS..."
sleep 5
sudo update-locale LANG=C LANGUAGE=C LC_ALL=C LC_MESSAGES=POSIX
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu trusty main" > /etc/apt/sources.list.d/ros-latest.list'
wget https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -O - | sudo apt-key add -
sudo apt-get -y update
sudo apt-get install -y ros-indigo-ros-base
sudo apt-get install -y ros-indigo-vision-opencv
sudo apt-get -y install python-rosdep
sudo rosdep init
rosdep update
echo "source /opt/ros/indigo/setup.bash" >> /home/pi/.bashrc
echo "alias xtion='roslaunch openni2_launch openni2.launch'" >> /home/pi/.bashrc
source /home/pi/.bashrc
sudo apt-get -y install python-rosinstall
echo "unset GTK_IM_MODULE" >> /home/pi/.bashrc
source /home/pi/.bashrc
sudo apt-get -y install ros-indigo-rgbd-launch ros-indigo-openni2-camera ros-indigo-openni2-launch
sudo apt-get -y install ros-indigo-rqt ros-indigo-rqt-common-plugins ros-indigo-rqt-robot-plugins
sudo apt-get -y install ros-indigo-rqt-gui ros-indigo-rqt-gui-cpp ros-indigo-rqt-image-view
sudo apt-get -y install ros-indigo-pcl-conversions ros-indigo-pcl-msgs ros-indigo-pcl-ros
sudo apt-get -y install ros-indigo-octomap ros-indigo-octomap-ros ros-indigo-octomap-msgs ros-indigo-octomap-mapping ros-indigo-libg2o
sudo apt-get -y install ros-indigo-rtabmap ros-indigo-rtabmap-ros

# Install OpenCV 2.4.9
echo "Installing OpenCV 2.4.9"
echo "Removing any pre-installed ffmpeg and x264"
sleep 5
sudo apt-get -y remove ffmpeg x264 libx264-dev
echo "Installing Dependenices"
sleep 5
sudo apt-get -y install libopencv-dev
sudo apt-get -y install build-essential checkinstall cmake pkg-config yasm
sudo apt-get -y install libtiff5-dev libjpeg-dev libjasper-dev libxine2-dev
sudo apt-get -y install libavcodec-dev libavformat-dev libswscale-dev libdc1394-22-dev libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev libv4l-dev
sudo apt-get -y install python-dev python-numpy
sudo apt-get -y install libqt4-dev libgtk2.0-dev
sudo apt-get -y install libfaac-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev
sudo apt-get -y install x264 v4l-utils
echo "OpenCV 2.4.9 ready to be used"

# Perform Modifications
echo "Modifying cmake file to point to correct OpenCV versions..."
sleep 5
sudo sed -i 's/.so.2.4.8/.so.2.4.9/g' /opt/ros/indigo/share/image_proc/cmake/image_procConfig.cmake
sudo sed -i 's/.so.2.4.8/.so.2.4.9/g' /opt/ros/indigo/share/cv_bridge/cmake/cv_bridgeConfig.cmake
sudo sed -i 's/.so.2.4.8/.so.2.4.9/g' /opt/ros/indigo/share/image_geometry/cmake/image_geometryConfig.cmake
sudo sed -i 's/.so.2.4.8/.so.2.4.9/g' /opt/ros/indigo/lib/pkgconfig/image_proc.pc
sudo sed -i 's/.so.2.4.8/.so.2.4.9/g' /opt/ros/indigo/lib/pkgconfig/cv_bridge.pc
sudo sed -i 's/.so.2.4.8/.so.2.4.9/g' /opt/ros/indigo/lib/pkgconfig/image_geometry.pc
sudo ln -s /usr/lib/arm-linux-gnueabihf/liblog4cxx.so /usr/lib/liblog4cxx.so

# Install serial
echo "Installing Serial package..."
sleep 5
cd /home/pi/
source /opt/ros/indigo/setup.bash
git clone https://github.com/wjwwood/serial.git
cd serial
sed -i -- 's/\/tmp//g' Makefile
make -j4
sudo make install
cd /home/pi/

# Install FlyNet code
echo "Installing FlyNet code..."
sleep 5
cd /home/pi/
mkdir -p lepton_tracking_ws/src
cd lepton_tracking_ws/src
git clone https://tsdean@bitbucket.org/cuflynet/lepton-serial.git
cd /home/pi/lepton_tracking_ws
source /home/pi/lepton_tracking_ws/src/lepton-serial/setup.sh
sed -i 's/2.4.11/2.4.9/g' /home/pi/lepton_tracking_ws/src/lepton-serial/CMakeLists.txt
catkin_make -j4

# Setup networking
sudo apt-get -y install dnsmasq iptables
echo "172.16.0.25     pi">> /etc/hosts
echo "auto eth0" >> /etc/network/interfaces
echo "iface eth0 inet static" >> /etc/network/interfaces
echo "address 172.16.0.1" >> /etc/network/interfaces
echo "gateway 172.16.0.1" >> /etc/network/interfaces
echo "dhcp-range=172.16.0.50,172.16.0.100,255.255.255.0,12h" >> /etc/dnsmasq.d/host
echo "interface=eth0" >> /etc/dnsmasq.d/host
echo "dhcp-host=b8:27:eb:cf:73:3d,pi,172.16.0.25,infinite" >> /etc/dnsmasq.d/host

# Final thoughts
echo "Dont forget to turn on SPI and I2C..."
echo "Go to Menu->Preferences->Raspberry Pi Configuration"
echo "Then REBOOT"
