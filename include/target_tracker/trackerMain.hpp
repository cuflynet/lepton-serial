/*
 * trackerMain.h
 *
 *  Created on: Oct 6, 2015
 *      Author: taylordean
 */

#ifndef TRACKERMAIN_HPP_
#define TRACKERMAIN_HPP_

#include <image_transport/image_transport.h>
#include <target_tracker/OBJECT_CONTAINER.hpp>
#include <cv_bridge/cv_bridge.h>


#define PI                              3.1415962153589
#define MAX_OBJECTS                     10
#define MIN_OBJECT_AREA                 1.0

#define FLIR_HFOV                       (51*PI/180.0)
#define FLIR_VFOV                       (37.83*PI/180.0)

#define DEPTH_HFOV                      (58*PI/180.0)
#define DEPTH_VFOV                      (45*PI/180.0)

#define FLIR_HPX                        80.0
#define FLIR_VPX                        60.0

#define DEPTH_HPX                       640.0
#define DEPTH_VPX                       480.0
#define DEPTH_FACTOR                    10.0

using namespace cv;
using namespace std;

void applyGain(Mat imgIn, Mat imgOut);
void getDepthImage(const sensor_msgs::ImageConstPtr& msg);
void getFlirImage(const sensor_msgs::ImageConstPtr& msg);
void morphImage(Mat imgIn, Mat& imgOut, int iLow, int iHigh);
void getContours(OBJECT_CONTAINER& objects, Mat imgContour);
void findCentroid(vector< Point > points, Point& centroid);
void processImages(Mat imgDepth, Mat imgOriginal, Mat& imgThresholded, OBJECT_CONTAINER& GLOBAL_RETURNS);
void doTracking(image_transport::Publisher irTrackedPub, image_transport::Publisher irMaskedPub, image_transport::Publisher depthTrackedPub, ros::Publisher targetPosPub);
#endif /* TRACKERMAIN_HPP_ */
