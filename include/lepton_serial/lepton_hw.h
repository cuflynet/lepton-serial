/* C++ interface to a serial output from the Lepton attached to a Teensy */
/* Built on top of https://github.com/wjwwood/serial serial library */

/*Split fcn from http://stackoverflow.com/questions/236129/split-a-string-in-c */

/* Need to implement a threaded queuing model, use cxx11 threading model
 */

#ifndef LEPTON_HW_DRIVER_H_
#define LEPTON_HW_DRIVER_H_

#include <string>
#include <iostream>
#include <unistd.h>
#include <serial/serial.h>
#include <stdlib.h>
#include <mutex>
#include <thread>
#include <queue>
#include <functional>

using namespace std;

template<typename T>
vector<T> split(const T & str, const T & delimiters)
{
  vector<T> v;
  typename T::size_type start = 0;
  size_t pos = str.find_first_of(delimiters, start);
  while(pos != T::npos) {
    if(pos != start) // ignore empty tokens
      v.push_back(str.substr(start, pos - start));
    start = pos + 1;
    pos = str.find_first_of(delimiters, start);
  }
  if(start < str.length()) // ignore trailing delimiter
    v.push_back(str.substr(start, pos - start)); // add what's left of the string
  return v;
}

class IRFrame
{
public:
  uint16_t height;
  uint16_t width;
  uint16_t* data;
  struct timespec frameTime;
  IRFrame()
  {
    height = width = 0;
    frameTime.tv_sec = frameTime.tv_nsec = 0;
    data = 0;
  }
  
  IRFrame(uint16_t m_width, uint16_t m_height)
  {
    height = m_height;
    width = m_width;
    frameTime.tv_sec = frameTime.tv_nsec = 0;
    data = new uint16_t[height*width*sizeof(uint16_t)];
  }

  //Copy an existing buffer
  IRFrame(uint16_t m_width, uint16_t m_height, uint16_t* buffer)
  {
    height = m_height;
    width = m_width;
    frameTime.tv_sec = frameTime.tv_nsec = 0;
    data = new uint16_t[height*width*sizeof(uint16_t)];
    memcpy(data, buffer, height*width*sizeof(uint16_t));
    clock_gettime(CLOCK_REALTIME, &frameTime);
  }
 
  ~IRFrame()
  {
    delete[] data;
  }
};

class LeptonDriver
{
public:
  LeptonDriver();
  ~LeptonDriver();
  int init(string portName, int baud);
  int initFake();
  
  //Caller frees
  int getFrame(IRFrame* &frame);

  static const int LEPTON_SUCCESS = 0;
  static const int LEPTON_FAILURE = 1;
  static const int LEPTON_TIMEOUT = 5000; //timeout in ms to receive data

private:
  mutex queueLock;
  thread *serviceThread;
  queue<IRFrame*> images;
  serial::Serial *mPort;
  bool shouldStop;

  void service();
  void serviceFake();
};


#endif
