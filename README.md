# README #

# Install and Build #
1. mkdir -p ~/lepton_tracking_ws/src
2. cd ~/lepton_tracking_ws/src
3. git clone https://tsdean@bitbucket.org/cuflynet/lepton-serial.git
4. cd ~/lepton_tracking_ws
5. source ~/lepton_tracking_ws/src/lepton-serial/setup.sh
6. catkin_make

# Run #
1. Open 3 terminals
2. Connect Flir Lepton to teensy 3.2
3. Connect teensy 3.2 to computer with micro-usb to usb
4. Ensure teensy 3.2 has correct code. If unsure, use Arduino IDE to install code located at ~/lepton_tracking_ws/devel/lib/lepton-serial/flir_lepton_teensy/flir_lepton.ino
5. terminal 1: roscore

On RPI
6. terminal 2: source ~/setOdroidMaster
R ~/lepton_tracking_ws/devel/lib/lepton-serial/lepton_spi
7. terminal 3: source ~/setOdroidMaster
~/lepton_tracking_ws/devel/lib/lepton-serial/target_tracker